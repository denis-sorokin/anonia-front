import router from './router'
import auth from '@/services/auth'
import store from '@/store'

router.beforeEach((to, from, next) => {
  const user = auth.getData()
  if (user.username) {
    store.dispatch('auth/LOGIN', user)
  }

  if (!user.username && to.path !== '/login') {
    next({ path: '/login' })
  } else if (user.token && (to.path === '/login')) {
    next({ path: '/' })
  } else {
    next()
  }
})
