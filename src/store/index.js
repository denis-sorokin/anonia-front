import Vue from 'vue'
import Vuex from 'vuex'

import stats from './module/stats'
import rooms from './module/rooms'
import auth from './module/auth'
import socket from './module/socket'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    stats,
    rooms,
    auth,
    socket
  }
})
