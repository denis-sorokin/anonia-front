
const state = {
  messages: {}
}

const getters = {
  messages: state => state.messages
}

const actions = {
  SOCKET_PING ({ commit }, msg) {
    commit('SOCKET_PING', msg)
  }
}

const mutations = {
  SOCKET_PING (state, msg) {
    state.messages = msg
  }
}

export default {
  state,
  actions,
  mutations,
  getters,
  namespaced: true
}
