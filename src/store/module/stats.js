
const state = {
  roomsCount: 3
}

const getters = {
  roomsCount: state => state.roomsCount
}

const actions = {
  GET_ROOMS_STATS ({ commit }, data) {
    // commit('ROOMS_STATS', data)
  }
}

const mutations = {
  ROOMS_STATS (state, data) {
    state.roomsCount = data.roomsCount
  }
}

export default {
  state,
  actions,
  mutations,
  getters,
  namespaced: true
}
