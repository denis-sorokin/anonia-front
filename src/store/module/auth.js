import auth from '@/services/auth'

const state = {
  user: {}
}

const getters = {
  info: state => state.user,
  username: state => state.user.username
}

const actions = {
  LOGIN ({ commit }, data) {
    commit('SAVE_USER', data)
    return true
  },
  LOGOUT ({ commit }) {
    commit('REMOVE_USER')
  }
}

const mutations = {
  SAVE_USER (state, data) {
    auth.setData(data)
    state.user = data
  },
  REMOVE_USER (state) {
    auth.removeData()
    state.user = {}
  }
}

export default {
  state,
  actions,
  mutations,
  getters,
  namespaced: true
}
