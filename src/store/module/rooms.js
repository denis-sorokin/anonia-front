
const state = {
  rooms: [
    {
      name: 'room-A',
      curr: 3,
      max: 5
    },
    {
      name: 'room-B',
      curr: 5,
      max: 5
    },
    {
      name: 'room-C',
      curr: 1,
      max: 5
    }
  ]
}

const getters = {
  all: state => state.rooms
}

const actions = {
  GET_ROOMS ({ commit }, data) {
    // commit('ROOMS_SAVE', data)
  }
}

const mutations = {
  ROOMS_SAVE (state, data) {
    state.rooms = data.rooms
  }
}

export default {
  state,
  actions,
  mutations,
  getters,
  namespaced: true
}
