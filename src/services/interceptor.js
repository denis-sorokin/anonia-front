import axios from 'axios'
import config from '../config'
import auth from './auth'

const api = axios.create({
  baseURL: `http://localhost:${config.SERVER_PORT}`,
  proxyHeaders: false,
  withCredentials: true,
  crossDomain: true
})

const errorClearToken = []

api.defaults.headers.common['Content-Type'] = 'application/json'
api.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

api.interceptors.request.use((config) => {
  if (auth.getData().token) {
    config.headers['Authorization'] = auth.getData().token
  }
  return config
}, (error) => {
  const msg = error.response.data.error.message
  return Promise.reject(msg)
})

api.interceptors.response.use((response) => {
  return response
}, (error) => {
  if (errorClearToken.includes(error.response.status)) {
    auth.removeData()
  }
  const msg = error.response.data.error.message
  return Promise.reject(msg)
})

export default api
