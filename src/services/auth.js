export default {
  getData () {
    return {
      username: localStorage.getItem('username')
    }
  },
  setData (user) {
    const { username } = user

    if (username) {
      localStorage.setItem('username', username)
    } else {
      console.error('Cannot set data to localStorage')
    }
  },
  removeData () {
    localStorage.clear()
  }
}
