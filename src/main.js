import Vue from 'vue'
import VueSocketio from 'vue-socket.io-extended'
import io from 'socket.io-client'

import App from './App.vue'
import router from './router'
import store from './store/index'

import cfg from '@/config'

import './authGuard'

Vue.use(VueSocketio, io(`${cfg.SOCKET_LINK}:${cfg.SOCKET_PORT}`))

// socket.on('connect', function () {
//   socket.send('pingServer', 'ping message from frontend!')
// })
//
// socket.on('pIng', function (msg) {
//   console.log(msg)
// })
//
// socket.on('stream', function (stream) {
//   const blob = new Blob([stream], { 'type': 'audio/ogg; codecs=opus' })
//   const audio = document.querySelector('#player')
//   audio.src = window.URL.createObjectURL(blob)
//   audio.play()
// })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
