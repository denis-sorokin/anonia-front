export default {
  SERVER_PORT: 11315,
  SOCKET_PORT: 11375,
  SOCKET_LINK: 'http://localhost'
}
