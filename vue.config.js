module.exports = {
  devServer: {
    open: true,
    host: '127.0.0.1',
    disableHostCheck: true,
    port: 3000,
    https: false,
    hotOnly: false,
    proxy: null,
    before: app => {
    }
  }
}
